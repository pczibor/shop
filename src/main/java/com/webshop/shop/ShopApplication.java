package com.webshop.shop;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.webshop.shop.data.Product;
import com.webshop.shop.data.ProductStorage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import java.io.File;
import java.util.List;

@SpringBootApplication
public class ShopApplication implements ApplicationRunner {
	@Value("${products}")
	private String products_path;

	@Autowired
	private ProductStorage productStorage = new ProductStorage();

	public static void main(String[] args) {
		SpringApplication.run(ShopApplication.class, args);
	}

	@Override
	public void run(ApplicationArguments args) throws Exception {
		List<Product> products;
		ObjectMapper ob = new ObjectMapper();
		products = ob.readValue(new File(products_path), new TypeReference<List<Product>>(){});

		for (Product p : products) {
			productStorage.addProduct(p);
		}
	}
}
