package com.webshop.shop.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class Recipe {
    @GetMapping("/recipe/{id}")
    public String viewRecipe(@PathVariable("id") String id) {
        return "recipe";
    }
}
