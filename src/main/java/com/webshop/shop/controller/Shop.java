package com.webshop.shop.controller;

import com.webshop.shop.data.OrderForm;
import com.webshop.shop.data.Product;
import com.webshop.shop.data.ProductStorage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import javax.validation.Valid;

@Controller
public class Shop {
    @Autowired
    private ProductStorage productStorage;

    @GetMapping("/shop")
    public String shopIndex(Model model) {
        model.addAttribute("products", productStorage.getProducts());
        return "shop";
    }

    @GetMapping("/shop/{product}")
    public String productDetail(@PathVariable("product") String product, @ModelAttribute("OrderForm") OrderForm orderForm, Model model) {
        Product p = productStorage.findByName(product);
        if (null != p) {
            model.addAttribute("product", p);
            return "productDetail";
        }

        return "redirect:/";
    }

    @PostMapping("/shop/")
    public String productOrder(@PathVariable("product") String product, @Valid OrderForm orderForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "redirect:/shop/" + product;
        }
        return "recipe";
    }
}
