package com.webshop.shop.data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class OrderForm {
    @NotNull
    @Min(1)
    @Max(100)
    private Integer quantity;

    @NotNull
    private String message;

    public Integer getQuantity() { return quantity; }
    public void setQuantity(Integer quantity) { this.quantity = quantity; }

    public String getMessage() { return message; }
    public void setMessage(String message) { this.message = message; }

    @Override
    public String toString() {
        return "OrderForm{" +
                "quantity=" + quantity +
                ", message='" + message + '\'' +
                '}';
    }
}
