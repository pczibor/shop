package com.webshop.shop.data;

import org.springframework.stereotype.Component;
import java.util.ArrayList;
import java.util.List;

@Component
public class ProductStorage {
    private List<Product> products = new ArrayList<>();

    public List<Product> getProducts() { return products; }
    public void setProducts(List<Product> products) { this.products = products; }
    public void addProduct(Product product) { this.products.add(product); }

    public Product findByName(String name) {
        for (Product p: products) {
            if (p.getName().equals(name)) {
                return p;
            }
        }
        return null;
    }
}